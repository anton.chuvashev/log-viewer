import subprocess
import shlex
from time import sleep
from parser import parse_line, row_to_hash
from termcolor import colored
import json
import sys
import re


container_name = ''
file_path = ''
filter_str = str('')


def print_usage():
    print('''
        usage
            python3 ftail.py [container id] [absolute path to file]
            python3 ftail.py 2f3k4f123f42 /var/log/file.log
    ''')

    exit(0)


def get_color_filter():
    file_name = open("static/client_console_filter.json")

    return json.load(file_name)


def start_docker_process():
    command = f"docker exec {container_name} tail -f {file_path}"
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)

    return process


def print_tail(text: str, color_filter: dict, filter_str: str, filter_hash: dict):
    result = parse_line(text, filter_str, filter_hash)

    for x in result:
        y = x

        for f in color_filter:
            split_re = re.compile(f[0])
            parts = split_re.split(x)
            matches = split_re.findall(x)

            if parts and matches:
                y = parts.pop()

                for xx in parts:
                    if type(f[-1]) == 'Dict':
                        attrs=f.pop
                        y += colored(matches.pop(), *f[1:], attrs=attrs) + xx
                    else:
                        y += colored(matches.pop(), *f[1:]) + xx

        print(y)


def loop_tail():
    color_filter = get_color_filter()
    filter_hash = row_to_hash(filter_str)

    process = start_docker_process()

    while True:
        output = process.stdout.readline()

        if output:
            print_tail(output.decode(), color_filter, filter_str, filter_hash)
        else:
            sleep(0.01)


if __name__ == '__main__':
    try:
        container_name = sys.argv[1]
        file_path = sys.argv[2]
    except IndexError:
        print_usage()

    if not container_name or not file_path:
        print_usage()

    loop_tail()
