import json
import re
import xml.dom.minidom


def read_log(file_name: str):
    result = []

    with open(file_name) as f:
        for line in f:
            result.append(line)

    return "".join(result)


def prettify_log(log: str, filter_str: str):
    filter_str = str(filter_str)

    result = []

    filter_hash = row_to_hash(filter_str)

    for line in log.splitlines():
        result += parse_line(line, filter_str, filter_hash)

    return "\n".join(result)


def parse_line(line: str, filter_str: str, filter_hash: dict):
    result = []

    if not len(line) \
            or len(filter_hash) and not filter_check_hash(line, filter_hash) \
            or not len(filter_hash) and not filter_check_str(line, filter_str):
        return result

    for ppt in json_to_rows(line, 0):
        offset = get_str_offset(ppt)

        if is_http_str(ppt):
            offset += get_indent()

            ppt = remove_escape_chars(ppt, offset)

            for l in ppt.splitlines():
                if re.search(r'\?xml', l):
                    result += xml_to_rows(l, offset + get_indent())
                elif l.count("=") and l.count("=") == l.count("&") + 1:
                    result += html_request_to_rows(l, offset)
                else:
                    result += json_to_rows(l, offset)
            offset -= get_indent()
        elif is_http_json_response_str(ppt):
            result += html_response_to_rows(ppt, offset)
        elif is_http_body_response_str(ppt):
            result += html_body_response_to_rows(ppt, offset)
        else:
            result += json_to_rows(ppt, offset)

    return result


def filter_check_hash(line: str, filter_hash: dict):
    data = row_to_hash(line)

    for key in filter_hash:
        if key in data and data[key] != filter_hash[key]:
            return 0

    return 1


def filter_check_str(line: str, filter_str: str):
    if not len(filter_str): return 1

    pattern = re.compile(filter_str)

    return re.search(pattern, line)


def json_to_rows(s: str, offset: int):
    result = []

    try:
        js = json.loads(s)
        out = json.dumps(js, sort_keys=True, indent=get_indent())
        for out_row in out.splitlines():
            result.append((" " * offset) + out_row)
    except:
        result.append(s)

    return result


def row_to_hash(s: str):
    try:
        js = json.loads(s)
    except:
        js = {}

    if not type(js) is dict: return {}

    return js


def xml_to_rows(xmls: str, offset: int):
    result = []

    xmls = re.sub(r'^\s+', '', xmls)

    try:
        dom = xml.dom.minidom.parseString(xmls)
        fxml = dom.toprettyxml(indent=" " * get_indent())
        fxml_lines = fxml.splitlines()

        for fxmll in fxml_lines:
            result.append((" " * offset) + fxmll)
    except Exception as e:
        print(e)
        result.append(xmls)

    result[0] = " " * (offset - get_indent()) + result[0]

    return result


def html_response_to_rows(req: str, offset: int):
    result = []

    for l in req.split("\\n"):
        ll = json_to_rows(l.replace('\\', ''), offset + get_indent())

        [result.append(x) for x in ll]

    return result


def html_body_response_to_rows(req: str, offset: int):
    result = ['"body": "']

    for l in req.split("\"body\": \""):
        l = l.replace('\\', '')
        l = re.sub(r'\",$', "", l)
        ll = json_to_rows(l, offset + get_indent())

        [result.append(x) for x in ll]

    result.append('",')

    return result


def html_request_to_rows(req: str, offset: int):
    result = []

    for l in req.split("&"):
        l = re.sub('^\s+', "", l)
        result.append((" " * offset) + l + " &")

    result[-1] = re.sub(r' &$', '', result[-1])

    return result


def is_http_str(s: str):
    return re.search(r'(HTTP|GET|POST|HEAD|\d{3}\s*[A-Z]{,7})', s)


def is_http_json_response_str(s: str):
    return re.search(r'\d{3}\s[A-Z]+\\n\\n', s)


def is_http_body_response_str(s: str):
    return re.search(r'"body":\s+"\{', s)


def remove_escape_chars(s: str, offset: int):
    s = re.sub(r'\\n', r'\n' + (" " * offset), s)
    s = re.sub(r'\\"', r'"', s)

    return s


def load_pretty_json(file_name: str):
    with open(file_name) as f:
        for line in f:
            js = json.loads(line)
            pt = json.dumps(js, sort_keys=True, indent=get_indent())

    return pt.splitlines()


def get_str_offset(s: str):
    offset = 0

    m = re.search(r'^\s+', s)
    if m:
        offset = len(m[0])

    return offset


def get_indent():
    return 4
