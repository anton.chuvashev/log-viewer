function escapeHTML(unsafeText) {
    let div = document.createElement('div');
    div.innerText = unsafeText;
    return div.innerHTML;
}

function App() {
  const [inputValue,      setInputValue]      = React.useState('');
  const [inputFilter,     setInputFilter]     = React.useState('');
  const [responseContent, setResponseContent] = React.useState('');
  const [responseResult,  setResponseResult]  = React.useState('');
  const [inProgress,      setInProgress]      = React.useState('');
  const [clientFilter,    setClientFilter]    = React.useState('');

  React.useEffect(() => {
    const fetchData = async () => {
        const data = await fetch('/static/client_filter.json');
        const json = await data.json();

        setClientFilter(json);
    }

    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setInProgress(1);

    const config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                text:   inputValue,
                filter: inputFilter
            }
        )
    }
    const response = await fetch(`/parse`, config);
    const data = await response.text();

    setResponseContent(data);
    applyStaticFilter(data);

    setInProgress(0);
  };

  const applyFilter = (text, filters) => {
    if (filters.length) {
        const filter  = filters.shift();
        const color   = filter[1];
        const pattern = filter[0];

        if (pattern) {
            var   chunks = text.split(pattern);
            const joiner = escapeHTML(pattern);

            for (let i = 0; i < chunks.length; i++) {
              chunks[i] = applyFilter(chunks[i], filters.map((x) => x));
            }

            const selected_style = [
                "border-bottom:solid 1px "+color,
                "color:" + color,
            ].join(';')

            return chunks.join("<span style='" + selected_style + "'>" + joiner + "</span>");
        }
        else {
            return applyFilter(text, filters.map((x) => x));
        }
    }
    else {
        return escapeHTML(text).replaceAll(/\s+/g, (x) => '&nbsp;'.repeat(x.length));
    }
  }

  const applyStaticFilter = (data) => {
    var filters = [
        [document.getElementById('filter-cli-purple').value, 'purple'],
        [document.getElementById('filter-cli-navy').value,   'navy'  ],
        [document.getElementById('filter-cli-blue').value,   'blue'  ],
        [document.getElementById('filter-cli-maroon').value, 'maroon'],
        [document.getElementById('filter-cli-olive').value,  'olive' ],
        [document.getElementById('filter-cli-green').value,  'green' ],
        [document.getElementById('filter-cli-teal').value,   'teal'  ],
    ];

    filters = filters.concat(clientFilter);

    setResponseResult(applyFilter(data || responseContent, filters).replaceAll('<br>', '<br/>'));
  };

  return (
    <form onSubmit={handleSubmit}>
        <div className="container">
            <div className="row">
                <div className="col-12 text-center">
                    <b>Log parser</b>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <textarea
                        className="col-12 text-secondary text_input"
                        rows="3"
                        id="textarea-in"
                        value={inputValue}
                        onChange={(e) => setInputValue(e.target.value)}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-9 col-lg-9 col-xl-10">
                    <input
                        className="text_input"
                        style={{"width":"100%"}}
                        id="filter-in"
                        value={inputFilter}
                        onChange={(e) => setInputFilter(e.target.value)}
                    />
                </div>
                <div className="col-3 col-lg-3 col-xl-2 text-end">
                    <button
                        className="form_button btn mb-1 btn-sm btn-outline-danger"
                        type="button"
                        onClick={(e) => setInputFilter('')}
                    >
                    <p className="p_button_left">&#9666;</p>
                    </button>
                    <button
                        className="form_button btn mb-1 btn-sm btn-outline-danger"
                        type="button"
                        onClick={(e) => setInputValue('')}
                    >
                    <p className="p_button_up">&#9650;</p>
                    </button>
                    <button className="form_button btn mb-1 btn-sm btn-outline-primary" type="submit">
                        <span className={inProgress ? 'spinner-border spinner-border-sm spinner_button' : 'visually-hidden'} role="status" aria-hidden="true"></span>
                        <p  className={inProgress ? 'visually-hidden' : 'p_button_down'}>&#9660;</p>
                    </button>
                </div>
            </div>
            <div className="row">
                <div className="col-9 col-lg-9 col-xl-10">
                    <input className="post_filter" style={{"color":"Purple","borderColor":"Purple"}} id="filter-cli-purple"/>
                    <input className="post_filter" style={{"color":"Navy","borderColor":"Navy"}}     id="filter-cli-navy"/>
                    <input className="post_filter" style={{"color":"Blue","borderColor":"Blue"}}     id="filter-cli-blue"/>
                    <input className="post_filter" style={{"color":"Maroon","borderColor":"Maroon"}} id="filter-cli-maroon"/>
                    <input className="post_filter" style={{"color":"Olive","borderColor":"Olive"}}   id="filter-cli-olive"/>
                    <input className="post_filter" style={{"color":"Green","borderColor":"Green"}}   id="filter-cli-green"/>
                    <input className="post_filter" style={{"color":"Teal","borderColor":"Teal"}}     id="filter-cli-teal"/>
                </div>
                <div className="col-3 col-lg-3 col-xl-2 text-end">
                    <button
                        className="form_button btn mb-1 btn-sm btn-outline-primary"
                        type="button"
                        onClick={(e) => applyStaticFilter()}
                    >
                        <p className="p_button_down">&#9660;</p>
                    </button>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-12">
                    <div
                        className="p-2 text-secondary text_input"
                        id="log-div"
                        dangerouslySetInnerHTML={{__html: responseResult}}
                    >
                    </div>
                </div>
            </div>
        </div>
    </form>
  );
}

const container = document.getElementById('app');
const root = ReactDOM.createRoot(container);
root.render(<App />);
