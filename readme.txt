Console:
    color filters: src/static/client_console_filter.json

    example:
        [
            ["\\d+", "light_green", "on_red", ["bold","underline"]],
            ...
        ]

    allowed colors:
        https://pypi.org/project/termcolor/

        black         on_black
        grey          on_grey
        red           on_red
        green         on_green
        yellow        on_yellow
        blue          on_blue
        magenta       on_magenta
        cyan          on_cyan
        light_grey    on_light_grey
        dark_grey     on_dark_grey
        light_red     on_light_red
        light_green   on_light_green
        light_yellow  on_light_yellow
        light_blue    on_light_blue
        light_magenta on_light_magenta
        light_cyan    on_light_cyan
        white         on_white

    allowed attrs:
        bold
        dark
        underline
        blink
        reverse
        consealed

    requirements:
        pip3 install -r requirements.txt

    run:
        cd src
        python3 ftail.py 4asd5f4a4asd5 /var/log/file.log
        ...
        ^C

Web:
    color filters: src/static/client_filter.json

    allowed colors:
        https://htmlcolorcodes.com/

    run:
        docker-compose build
        docker-compose up

        http://127.0.0.1:5002/

    first field - your unparsed log file

    2nd field - server-side lines filter: json of 1'st level keys to match, or regexp

    red buttons - remove fields content

    blue top button - parse log

    colourful inputs - client-side color highlighting by simple string-match

    blue bottom button - refrech color highlight

    src/static/client_filter.json - custom-highlight
